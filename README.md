# Discord / IRC Bridge

Right now it's all set up in a WIP state on https://irc.connolly.tech/
You can use the kiwi web client there or connect with your own client.

Lots of code cleanup is needed! And it would be ideal to use config files, so the bot can be used with any Discord server!