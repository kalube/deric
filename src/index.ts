import * as Discord from "discord.js";
import fs from "fs";
import * as irc from "irc";
const discordClient = new Discord.Client();
const ircClient = new irc.Client('irc.connolly.tech', 'D', {
    userName: "Deric",
    realName: "The friendly Discord IRC Bot",
    channels: ['#sucs-test', '#sucs-general']
});

var channels: (Discord.TextChannel | Discord.DMChannel)[] = [];

/**
 * Custom types
 */
interface IRCMessage {
    user: string;
    isbot: boolean;
    content: string;
    target: string;
}

discordClient.on('ready', () => {
    console.log(`[DC] Logged in as ${discordClient.user.tag}!`);
    discordClient.user.setPresence({
        activity: {
            type: "WATCHING",
            name: "You type...",
            url: "https://connolly.tech"
        },
        afk: true
    })
});

discordClient.on('message', (message: Discord.Message) => {
    if (message.author.bot) return;
    switch (message.channel.id) {
        case "400973400111185921": //#general
            if (!channels.find(chan => chan.id == "400973400111185921")) channels.push(message.channel);
            ircSendMessage({
                user: message.member.nickname+` (${message.author.tag})`,
                isbot: message.author.bot,
                content: message.content,
                target: "#sucs-general"
            });
            break;
        case "681031714977742887": //#test - only for testing the bot
            if (!channels.find(chan => chan.id == "681031714977742887")) channels.push(message.channel);
            ircSendMessage({
                user: message.member.nickname+` (${message.author.tag})`,
                isbot: message.author.bot,
                content: message.content,
                target: "#sucs-test"
            });
            break;
        default:
            break;
    }
});

ircClient.addListener('message#sucs-test', (from, message) => {
    console.log(from+": "+message);
    let chan = channels.find(chan => chan.id == "681031714977742887");
    if (!chan) return;
    chan.send(`**<${from}>** ${message}`);
});
ircClient.addListener('message#sucs-general', (from, message) => {
    console.log(from+": "+message);
    let chan = channels.find(chan => chan.id == "400973400111185921");
    if (!chan) return;
    chan.send(`**<${from}>**: ${message}`);
})

let api_token: string = process.env.DISCORD_TOKEN || ""; //empty if undefined
if (api_token.length == 0) {
    api_token = fs.readFileSync('tokens', 'utf8').split(/\r?\n/)[0]; //split on newline
}
discordClient.login(api_token);

function ircSendMessage(message: IRCMessage) {
    ircClient.say(message.target, 
        `${message.user}: ${message.content}`)
}