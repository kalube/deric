"use strict";
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const Discord = __importStar(require("discord.js"));
const fs_1 = __importDefault(require("fs"));
const irc = __importStar(require("irc"));
const discordClient = new Discord.Client();
const ircClient = new irc.Client('irc.connolly.tech', 'D', {
    userName: "Deric",
    realName: "The friendly Discord IRC Bot",
    channels: ['#sucs-test', '#sucs-general']
});
var channels = [];
discordClient.on('ready', () => {
    console.log(`[DC] Logged in as ${discordClient.user.tag}!`);
    discordClient.user.setPresence({
        activity: {
            type: "WATCHING",
            name: "You type...",
            url: "https://connolly.tech"
        },
        afk: true
    });
});
discordClient.on('message', (message) => {
    if (message.author.bot)
        return;
    switch (message.channel.id) {
        case "400973400111185921": //#general
            if (!channels.find(chan => chan.id == "400973400111185921"))
                channels.push(message.channel);
            ircSendMessage({
                user: message.member.nickname + ` (${message.author.tag})`,
                isbot: message.author.bot,
                content: message.content,
                target: "#sucs-general"
            });
            break;
        case "681031714977742887": //#test - only for testing the bot
            if (!channels.find(chan => chan.id == "681031714977742887"))
                channels.push(message.channel);
            ircSendMessage({
                user: message.member.nickname + ` (${message.author.tag})`,
                isbot: message.author.bot,
                content: message.content,
                target: "#sucs-test"
            });
            break;
        default:
            break;
    }
});
ircClient.addListener('message#sucs-test', (from, message) => {
    console.log(from + ": " + message);
    let chan = channels.find(chan => chan.id == "681031714977742887");
    if (!chan)
        return;
    chan.send(`**${from}**: ${message}`);
});
ircClient.addListener('message#sucs-general', (from, message) => {
    console.log(from + ": " + message);
    let chan = channels.find(chan => chan.id == "400973400111185921");
    if (!chan)
        return;
    chan.send(`**${from}**: ${message}`);
});
let api_token = process.env.DISCORD_TOKEN || ""; //empty if undefined
if (api_token.length == 0) {
    api_token = fs_1.default.readFileSync('tokens', 'utf8').split(/\r?\n/)[0]; //split on newline
}
discordClient.login(api_token);
function ircSendMessage(message) {
    ircClient.say(message.target, `${message.user}: ${message.content}`);
}
//# sourceMappingURL=index.js.map